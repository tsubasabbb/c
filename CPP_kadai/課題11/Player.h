class Player
{
	//メンバ変数
	int hp;
	int atk;
	int def;
	

	//メンバ関数
public:
	Player();
	int Attack(int i);
	void DispHp();
	void Damage(int i);
	int GetDef();
	bool IsDead();
};
