class Enemy
{
	//メンバ変数
	int hp;
	int atk;
	int def;

	//メンバ関数
public:
	Enemy();
	void DispHp();
	int Attack(int i);
	void Damage(int i);
	int GetDef();
	bool IsDead();
};