#include"Fat.h"

//定数宣言
#define NUM 5
//defineは定義させるという意味

//プロトタイプ宣言
void SetData();
void BmiCalc();
void Sort();
void Output();

//グローバル変数
Fat *human; //クラスのポインタ

//メイン関数
int main()
{
	//インスタンス配列を作成
	human = new Fat[NUM];  //human = Fat.hは×
	SetData();
	BmiCalc();
	Sort();
	Output();

	//インスタンスを削除
	delete human;
}


//全員分の名前、身長、体重を設定
void SetData()
{
	//全員分のデータ
	char* name[] = { "太郎","花子","次郎","良子","吾郎" }; //名前
	float haight[] = { 1.72f,1.63f,1.85f,1.56f,1.77f }; //身長
	float weight[] = { 68.2f,55.6f,92.5f,63.3f,50.1f };

	//アクセス関数を使って全員分のデータを渡す
	for (int i = 0; i < NUM; i++)
	{
		human[i].SetName(name[i]);
		human[i].SetHeight(height[i]);
		human[i].SetWeight(weight[i]);
		//データにSetをつけてかっこの中にiをつけたデータにする
	}

}

//全員分のBMIを計算
void BmiCalc()
{
	for (int i = 0; i < NUM; i++)
	{
		human[i].Calc();
	}
}