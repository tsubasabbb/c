#include"Calclation.h"

//プロトタイプ宣言
void SetX(float a, float b);
void SetY(float a, float b);

//グローバル変数
Calclation x, y;

//メイン変数
int main()
{
	//インスタンスの処理
	SetX(5.0, 10.0);
	x.Disp();

	//インスタンスYの処理
	SetY(8.0, 3.0);
	y.Disp();
}

//インスタンスXのアクセス関数を呼ぶ
void SetX(float a, float b)
{
	x.SetA(a);
	x.SetB(b);
}
void SetY(float a, float b)
{
	y.SetA(a);
	y.SetB(b);
}