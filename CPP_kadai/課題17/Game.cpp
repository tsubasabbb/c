﻿
#include <iostream>
#include "Player.h"
#include "Enemy.h"
#include "Game.h"

void Game::GameLoop()
{
	Player Pl; 

	Enemy Ene; 

	int damage; 

	const char* Player = "Pl";
	const char* Enemy = "Ene";
	for (int turn = 1; ; turn++)
	{
		std::cout << "Pl\n" << turn << "Ene\n";

		Pl.DispHp(Player);
		Ene.DispHp(Enemy);

		damage = Pl.Attack(Ene.GetDef()); 
		Ene.Damage(damage, Player); 
		if (Ene.IsDead())
		{
			break; 
		}
		
		
		damage = Ene.Attack(Pl.GetDef()); 
		Pl.Damage(damage, Enemy);
		if (Pl.IsDead())
		{
			break; 
		}
	}
}